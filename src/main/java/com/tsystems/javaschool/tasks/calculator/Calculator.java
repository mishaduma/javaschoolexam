package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

import static java.math.RoundingMode.HALF_UP;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private static final String operators = "+*-/";

    public String evaluate(String statement) {
        try {
            double result = calculate(postfix(statement));
            return new BigDecimal(result).setScale(4, HALF_UP).stripTrailingZeros().toPlainString();
        } catch (Exception e) {
            return null;
        }
    }

    private static int precedence(String operator) {
        switch (operator) {
            case "+":
            case "-":
                return 0;
            case "*":
            case "/":
                return 1;
            default:
                return 2;
        }
    }

    private static String postfix(String infix) {
        StringBuilder output = new StringBuilder();
        Deque<String> stack = new LinkedList<>();

        for (String ch : infix.split("")) {
            if (operators.contains(ch)) {
                output.append(' ');
                while (!stack.isEmpty()
                        && (precedence(ch) <= precedence(stack.peek())) && precedence(stack.peek()) < 2) {
                    output.append(stack.pop()).append(' ');
                }
                stack.push(ch);

            } else if (ch.equals("(")) {
                stack.push(ch);
            } else if (ch.equals(")")) {
                output.append(' ');
                while (!stack.peek().equals("(")) {
                    output.append(stack.pop());
                }
                stack.pop();
            } else {
                output.append(ch);
            }
        }
        output.append(' ');
        while (!stack.isEmpty())
            output.append(stack.pop()).append(' ');
        return output.toString();
    }

    private static double calculate(String postfix) {
        Stack<Double> numbers = new Stack<>();
        for (String elem : postfix.split(" ")) {
            if (!operators.contains(elem)) {
                numbers.push(Double.valueOf(elem));
            } else {
                switch (elem) {
                    case "+":
                        numbers.push(numbers.pop() + numbers.pop());
                        break;
                    case "-":
                        numbers.push(-numbers.pop() + numbers.pop());
                        break;
                    case "*":
                        numbers.push(numbers.pop() * numbers.pop());
                        break;
                    case "/":
                        double tmp = numbers.pop();
                        numbers.push(numbers.pop() / tmp);
                        break;
                }
            }
        }
        return numbers.pop();
    }
}