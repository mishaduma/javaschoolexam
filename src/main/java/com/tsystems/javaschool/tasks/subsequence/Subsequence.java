package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        ArrayList<Integer> index = new ArrayList<>();
        try {
            checkInput(x, y);
            for (int i = 0; i < x.size(); i++) {
                index.add(y.indexOf(x.get(i)));
                if (x.size() == 0 || x == null) return true;
            }

            for (int i = 0; i < index.size() - 1; i++) {
                if (!(index.get(i) < index.get(i + 1))) {
                    return false;
                }
            }
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return true;
    }

    private void checkInput(List x, List y) {
        if (x == null && y.size() == 0) {
            throw new IllegalArgumentException();
        }
        if (y == null && x.size() == 0) {
            throw new IllegalArgumentException();
        }
    }
}