package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
            int height = findHeight(inputNumbers.size());
            int width = height * 2 - 1;
            int[][] array = new int[height][width];
            Iterator<Integer> iterator = inputNumbers.iterator();

            for (int i = 0; i < height; i++) {
                for (int m = width - height - i; m <= width - height + i; m++) {
                    array[i][m++] = iterator.next();
                }
            }
            return array;
        } catch (Throwable e) {
            throw new CannotBuildPyramidException();
        }
    }

    private int findHeight(int size) {
        int i = 1;
        int height = 0;
        while ((size = size - i) > 0) {
            height++;
            i++;
        }
        if (size < 0) throw new CannotBuildPyramidException();
        else return height + 1;
    }
}